;;; kakoune.el --- A simulation, but not emulation, of kakoune -*- lexical-binding: t; -*-

;; Author: Joseph Morag <jm4157@columbia.edu>
;; Version: 0.1
;; URL: https://github.com/jmorag/kakoune.el
;; Package-Requires: ((ryo-modal "0.45") (multiple-cursors "1.4") (expand-region "0.11.0") (emacs "25.1"))
;; MIT License

;;; Commentary:
;; This package provides many, but not all of the editing primitives in the kakoune editor.
;; Unlike evil-mode for vim, this is very shallow emulation, and seeks to do as little
;; work as possible, leveraging Emacs native editing commmands and the work of other
;; packages wherever possible.

;;; Code:
(require 'kakoune-utils)
(require 'kakoune-exchange)
(require 'kakoune-unimpaired)
(require 'kakoune-shell-commands)
(require 'cl-lib)
(require 'ryo-modal)
(require 'expand-region)
(require 'multiple-cursors)

;;;###autoload

;; It's probably impossible to recreate the functionality of selection/cursor dropping to next line from kakoune using mc, probably need something like zones.el
;; For now just bind C as rectangular-region-anchor
	;(set-rectangular-region-anchor)
	;(next-line)
	;(rectangular-region-mode 0) NOT WANTED
	;(multiple-cursors-mode 1)
  ;(multiple-cursors-mode)
	;(rrm/switch-to-multiple-cursors)
	;(newline) ;to find key, do describe-key while rectangular-region-anchor is active
;)
(defun kakoune-setup-keybinds ()
  "Set up default kakoune keybindings for normal mode."
  (global-subword-mode 1)
  (ryo-modal-keys
   (:mc-all t)
   ;; Region selectors
   ("M-i" (("w" er/mark-word)
           ("b" er/mark-inside-pairs)
           ("'" er/mark-inside-quotes)))
   ("M-a" (("w" er/mark-symbol)
           ("b" er/mark-outside-pairs)
           ("'" er/mark-outside-quotes))))
  ;; this works now but I've gotten used to not having it
   (ryo-modal-major-mode-keys 'prog-mode
    ("b" kakoune-backward-same-syntax :first '(kakoune-set-mark-here) :mc-all t)
    ("B" kakoune-backward-same-syntax :first '(kakoune-set-mark-if-inactive) :mc-all t)
    ("w" forward-same-syntax :first '(kakoune-set-mark-here) :mc-all t)
    ("W" forward-same-syntax :first '(kakoune-set-mark-if-inactive) :mc-all t))
  (ryo-modal-keys
    ;;Basic keybindings (single cursor)
    (:mc-all 0)
    ("o" kakoune-o :mc-all 0 :exit t)
    ("O" kakoune-O :exit t))
  (ryo-modal-keys
   (:mc-all t)
   ;; Basic keybindings
   ("a" forward-char :exit t :mc-all t)
   ("A" move-end-of-line :exit t :mc-all t)
   ("b" backward-word :first '(kakoune-set-mark-here) :mc-all t)
   ("B" backward-word :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("c" kakoune-d :exit t :mc-all t)
   ;("C" kill-line :exit t :mc-all t)
	 ("C" set-rectangular-region-anchor :mc-all t)
   ("d" kakoune-d :mc-all t)
   ("D" kill-line :mc-all t)
   ("f" kakoune-select-to-char :first '(kakoune-set-mark-here) :mc-all t)
   ("F" kakoune-select-to-char :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("g" (("h" beginning-of-line :mc-all t)
         ("<left>" beginning-of-line :mc-all t)
         ("j" end-of-buffer :mc-all t)
         ("<down>" end-of-buffer :mc-all t)
         ("k" beginning-of-buffer :mc-all t)
         ("<up>" beginning-of-buffer :mc-all t)
         ("g" kakoune-gg :mc-all t)
         ("l" end-of-line :mc-all t)
         ("<right>" end-of-line :mc-all t)
         ("i" back-to-indentation :mc-all t)) :first '(kakoune-deactivate-mark) :mc-all t)
   ("G" (("h" beginning-of-line :mc-all t)
         ("<left>" beginning-of-line :mc-all t)
         ("j" end-of-buffer :mc-all t)
         ("<down>" end-of-buffer :mc-all t)
         ("k" beginning-of-buffer :mc-all t)
         ("<up>" beginning-of-buffer :mc-all t)
         ("g" kakoune-gg :mc-all t)
         ("l" end-of-line :mc-all t)
         ("<right>" end-of-line :mc-all t)
         ("i" back-to-indentation :mc-all t)) :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("g f" find-file-at-point :mc-all t)
   ("G f" find-file-at-point :mc-all t)
   ("g x" kakoune-exchange :mc-all t)
   ("g X" kakoune-exchange-cancel :mc-all t)
   ("h" backward-char :first '(kakoune-deactivate-mark) :mc-all t)
   ("H" backward-char :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("i" kakoune-insert-mode :mc-all t)
   ("I" back-to-indentation :exit t :mc-all t)
   ("j" next-line :first '(kakoune-deactivate-mark) :mc-all t)
   ("J" next-line :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("k" previous-line :first '(kakoune-deactivate-mark) :mc-all t)
   ("K" previous-line :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("l" forward-char :first '(kakoune-deactivate-mark) :mc-all t)
   ("L" forward-char :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ;("o" kakoune-o :exit t) ;mc-all 0 would be good also
   ;("O" kakoune-O :exit t)
   ("p" kakoune-p :mc-all t)
   ("r" kakoune-replace-char :mc-all t)
   ("R" kakoune-replace-selection :mc-all t)
   ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here) :mc-all t)
   ("T" kakoune-select-up-to-char :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("w" forward-word :first '(kakoune-set-mark-here) :mc-all t)
   ("W" forward-word :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("M-w" forward-symbol :first '(kakoune-set-mark-here) :mc-all t)
   ("M-W" forward-symbol :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("x" kakoune-x :mc-all t)
   ("X" kakoune-X :mc-all t)
   ("y" kill-ring-save :mc-all t)
   ("Y" kakoune-Y :mc-all t)
   ("." kakoune-select-again :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("M-;" exchange-point-and-mark :mc-all t)
   ("`" kakoune-downcase :mc-all t)
   ("~" kakoune-upcase :mc-all t)
   ("%" mark-whole-buffer :mc-all t)
   ("M-j" kakoune-join :mc-all t)
   ("[ [" backward-paragraph :first '(kakoune-set-mark-here) :mc-all t)
   ("] ]" forward-paragraph :first '(kakoune-set-mark-here) :mc-all t)
   (">" kakoune-indent-right :mc-all t)
   ("<" kakoune-indent-left :mc-all t)

   ;; Treat arrow keys the same as "hjkl"
   ("<down>" next-line :first '(kakoune-deactivate-mark) :mc-all t)
   ("<S-down>" next-line :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("<up>" previous-line :first '(kakoune-deactivate-mark) :mc-all t)
   ("<S-up>" previous-line :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("<right>" forward-char :first '(kakoune-deactivate-mark) :mc-all t)
   ("<S-right>" forward-char :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("<left>" backward-char :first '(kakoune-deactivate-mark) :mc-all t)
   ("<S-left>" backward-char :first '(kakoune-set-mark-if-inactive) :mc-all t)

   ;; Keep selection with S-<home> and S-<end>
   ("S-<home>" beginning-of-visual-line :first '(kakoune-set-mark-if-inactive) :mc-all t)
   ("S-<end>"  end-of-visual-line :first '(kakoune-set-mark-if-inactive) :mc-all t)

   ;; Numeric arguments
   ("0" "M-0" :norepeat t :mc-all t)
   ("1" "M-1" :norepeat t :mc-all t)
   ("2" "M-2" :norepeat t :mc-all t)
   ("3" "M-3" :norepeat t :mc-all t)
   ("4" "M-4" :norepeat t :mc-all t)
   ("5" "M-5" :norepeat t :mc-all t)
   ("6" "M-6" :norepeat t :mc-all t)
   ("7" "M-7" :norepeat t :mc-all t)
   ("8" "M-8" :norepeat t :mc-all t)
   ("9" "M-9" :norepeat t :mc-all t)
   ("-" "M--" :norepeat t :mc-all t)

   ;; Unimpaired-like functionality
   ("[" (("SPC" kakoune-insert-line-above :mc-all t)
         ("p" kakoune-paste-above :mc-all t)))
   ("]" (("SPC" kakoune-insert-line-below :mc-all t)
         ("p" kakoune-paste-below :mc-all t)))

   ;; Multiple cursors
   ("s" mc/mark-all-in-region :mc-all t)
   ("S" mc/split-region :mc-all t)

   ;; Shell commands
   ("|" kakoune-shell-pipe :mc-all t)
   ("!" kakoune-shell-command :mc-all t))

  ;; put these here because they shouldn't be repeated for all cursors
  (ryo-modal-keys
   ("[ b" previous-buffer :mc-all 0)
   ("] b" next-buffer :mc-all 0)))

(provide 'kakoune)
;;; kakoune.el ends here
